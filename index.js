import BinarySearch    from './src/Algo/BinarySearch.js'
import BoxMuller       from './src/Random/BoxMuller.js'
import MersenneTwister from './src/Random/MersenneTwister.js'
import Sampler         from './src/Random/Sampler.js'

import Utils           from './src/Utils.js'
import {
  range, unzip, zip,
}                      from './src/Utils.js'

import Ops             from './src/Ops.js'
import {
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr, // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
  binAdd, add,
  binMul, mul,
  binPow, pow,
  binMinus, minus,
  binDivInt, divInt,
  binDivFloat, divFloat,
  binDiv, div,
  binLog, log,
  binRoot, root,
}                      from './src/Ops.js'

export default {
  BinarySearch,
  BoxMuller,
  MersenneTwister,
  Sampler,

  Utils,
  // --------------------------------------------------
  range, unzip, zip,

  Ops,
  // --------------------------------------------------
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr, // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
  binAdd, add,
  binMul, mul,
  binPow, pow,
  binMinus, minus,
  binDivInt, divInt,
  binDivFloat, divFloat,
  binDiv, div,
  binLog, log,
  binRoot, root,
}

export {
  BinarySearch,
  BoxMuller,
  MersenneTwister,
  Sampler,

  Utils,
  // --------------------------------------------------
  range, unzip, zip,

  Ops,
  // --------------------------------------------------
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr, // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
  binAdd, add,
  binMul, mul,
  binPow, pow,
  binMinus, minus,
  binDivInt, divInt,
  binDivFloat, divFloat,
  binDiv, div,
  binLog, log,
  binRoot, root,
}


# Node Utils

Small utilities like random sampler for Node

## Install

As of now this repository has not been published to `npm`.  However, the repository is public, and can be installed using git as such.

```sh
npm i "https://gitlab.com/bvraghav/node_utils"
```

## Usage

### Random
```js
import { BoxMuller } from '@bvraghav/node_utils/Random/BoxMuller.js'
import { MersenneTwister } from '@bvraghav/node_utils/Random/MersenneTwister.js'
import { Sampler } from '@bvraghav/node_utils/Random/Sampler.js'
import { twoWordNames, twoWordName, } from '@bvraghav/node_utils/Random/TwoWordNames.js'
```
The simplest way to generate random numbers is through the `Sampler`

```js
import { Sampler } from '@bvraghav/node_utils/Random/Sampler.js'

const seed = Date.now()
const sampler = new Sampler(seed)
const xListOfTwo = sampler.sample(
    [1,2,3,4,5,6,7,8,9,],
    {k:2},
)
```

#### Reference
```js
class Sampler {
    constructor(seed)
    random ()
    normal ()
    randInt ()
    randRange(a, bExclusive)
    randRangeIn(a, b)

    // Sample from a distribution
    // ------------------------------------------------
    // seq: Arr[any] | sequence
    // W: Arr[float] | Weight vector (uniform by default)
    // k: Integer | sample size
    // rep: Boolean | with/without replacement
    sample(seq, {W, k, rep}={k:1, rep:false})

    sampleOne(seq, {W})
    sampleIid(seq, {W, k})
    choice(seq)
    shuffle(seq)
    sampleUniformIid(seq, k)
    sampleUniform(seq, k)
}
```

### Ops

Convenience Ops for functional composition

```js
import {
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr,  // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
  binAdd, add,
  binMul, mul,
  binPow, pow,
  binMinus, minus,
  binDivInt, divInt,
  binDivFloat, divFloat,
  binDiv, div,
  binLog, log,
  binRoot, root,
} from '@bvraghav/node_utils/Ops.js'
```

### Algo

Binary Search

```js
import {
  findGlb,
  findLub,
  BinarySearch,
} from '@bvraghav/node_utils/Algo/BinarySearch.js'
```

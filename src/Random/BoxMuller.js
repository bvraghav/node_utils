import {MersenneTwister} from '#src/Random/MersenneTwister'

class _BoxMuller {
  #prng = null
  #gen = null

  constructor(prng) {
    this.#prng = prng
    this.#gen = this.generator()
    this.normal = () => this.#gen.next().value
  }

  *generator() {
    let u,v,r,ct,st

    while (true) {

      // Taken from
      // https://stackoverflow.com/a/36481059
      u = 1 - this.#prng.random();
      v = this.#prng.random();
      r = Math.sqrt( -2.0 * Math.log( u ) )
      ct = Math.cos( 2.0 * Math.PI * v )

      yield r * ct
      
      // st = Math.sqrt(1 - ct*ct)
      // yield r * st
    }
  }
}

export class BoxMuller extends _BoxMuller {
  constructor(seedOrPrng) {
    if (!seedOrPrng || typeof seedOrPrng == 'number') {
      seedOrPrng = new MersenneTwister(seedOrPrng)
    }

    super(seedOrPrng)
  }
}

export default BoxMuller 

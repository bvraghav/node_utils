import { MersenneTwister } from '#src/Random/MersenneTwister'
import { BoxMuller } from '#src/Random/BoxMuller'

export class Sampler {
  #prng = null
  #nt = null

  constructor(seed) {
    this.#prng = new MersenneTwister(seed)

    this.#nt = new BoxMuller(this.#prng)

  }

  random () {
    return this.#prng.random()
  }

  normal () {
    return this.#nt.normal()
  }

  randInt () {
    return this.randRange()
  }


  randRange(a, bExclusive) {
    const r = this.#prng.genrand_int32() >>>0

    if (!a && !bExclusive) return r

    if (!bExclusive) {
      bExclusive = a
      a = 0
    }

    bExclusive -= a
    bExclusive &= (~0 >>>0)     // % (1<<32 - 1)
    return a + r % bExclusive
  }

  randRangeIn(a, b) {
    return this.randRange(a, 1+b)
  }


  sample(seq, {W, k, rep}={k:1, rep:false}) {

    // Sample from a distribution
    // ------------------------------------------------
    // seq: Arr[any] | sequence
    // W: Arr[float] | Weight vector (uniform by default)
    // k: Integer | sample size
    // rep: Boolean | with/without replacement

    if (!W || W.length < 1) {
      return (
        rep
          ? this.sampleUniformWirep(seq, k)
          : this.sampleUniform(seq, k)
      )
    }

    // const _sum = (a,b) => (a+b)
    const _cum = (X) => {
      let s=0.
      return X.map((x) => (s += x))
    }

    let C, p
    , src = [...Array(seq.length).keys()]
    , dst = []
    , flip = (seq.length >> 2) < k


    W = Array.from(W)

    while (dst.length < k) {
      if ( !rep || dst.length < 1) {

        if (0 < dst.length) {
          W.splice(p, 1)
        }

        // Normalise
        // --------------------------------------------
        // W has been mutated or is entering first time
        C = _cum(W)
        C = C.map((x) => (x / C.at(-1)))
      }

      p = C.findIndex((x) => (this.random() < x))
      dst.push(...src.splice(p, 1))

    }

    return dst.map(i => seq[i])
  }

  sampleOne(seq, {W}) {
    return this.sample(seq, {W}).pop()
  }

  sampleIid(seq, {W, k}) {
    return this.sample(seq, {W,k,rep:true})
  }

  choice(seq) {
    return this.sampleOne(seq)
  }

  shuffle(seq) {
    return this.sample(seq, {k:seq.length})
  }

  sampleUniformIid(seq, k) {
    const src = [...Array(k).keys()]
    const dst = []

    // SRC mutated by the closure to forEach; hence copy.
    Array.from(src).forEach(
      () => {
        dst.push(...src.splice(
          this.randRange(seq.length), 1
        ))
      }
    )

    return dst.map(i => seq[i])
  }

  sampleUniform(seq, k) {
    k = Math.min(k, seq.length)
    const src = [...Array(seq.length).keys()]
    const dst = []

    // Copy before [SRC] reversed is inplace
    Array(k).fill(0).forEach(
      () => {
        const p = this.randRange(src.length)
        dst.push(...(src.splice(p, 1)))
      }
    )

    return dst.map(i => seq[i])
  }
}

export default Sampler

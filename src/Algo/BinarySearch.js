import Ops from '#src/Ops'

export const dOpt = {
  eq: Ops.eq,
  less:Ops.lt,
  s:0,
  e:-1,
}

export function findGlb(
  P, x, {s, e, less}=dOpt) {
  // GLB is defined as P[m-1] < x <= P[m] which means
  // that any arbitrarily small x will satisfy x <=
  // P[m=0].

  const  start = () => (s<0?0:(P.length<=s?P.length-1:s))
  , end = () => (e<0?P.length:(P.length<e?P.length:e))
  , mid = () => (end()+start())>>1
  
  if (end() < start()) return -1

  while (! less(P[end()-1], x)) {
    while (less (P[mid()], x)) {
      s = 1+ mid()
    }
    e = mid()
  }

  return e
}

export function findLub(
  P, x, {s, e, less}=dOpt) {
  // LUB is defined as P[m-1] <= x < P[m] which means
  // that any arbitrarily small x will satisfy x <
  // P[m=0].

  const  start = () => (s<0?0:(P.length<=s?P.length-1:s))
  , end = () => (e<0?P.length:(P.length<e?P.length:e))
  , mid = () => (end()+start())>>1
  
  if (end() < start()) return -1

  while (less(x, P[end()-1])) {
    while (!less(x, P[mid()])) {
      s = 1+mid()
    }
    e = mid()
  }

  return e
}

export function findMember(P,x,{s,e,less,eq}=dOpt) {

  const  start = () => (s<0?0:(P.length<=s?P.length-1:s))
  , end = () => (e<0?P.length:(P.length<e?P.length:e))
  , mid = () => (end()+start())>>1
  
  if (less(x, P[start()]) || less(P[end()], x))
    return -1

  if (eq(P[start()],x)) return start()
  if (eq(P[end()-1], x)) return end()

  s = 1+start()
  e = end() - 1
  while (start() < end()) {
    if (eq(x,P[mid()])) return mid()
    if (less(x, P[mid()]))
      e = mid()                 // by convention end is
                                // one after the last
    else
      s = 1+mid()               // by convention starts
                                // at first
  }

  return -1
}

export class BinarySearch {
  P = [...Array(100).keys()]
  x = 17
  #glb = null
  #lub = null

  constructor(P, x, less=Ops.lt) {
    this.P = P
    this.x = x
    this.less = less
  }

  get lowerBoundIndex() {
    if (! this.#glb) {
      this.#glb = findGlb(
        this.P, this.x, {...dOpt, less: this.less}
      )
    }

    return this.#glb
  }

  get upperBoundIndex() {
    if (! this.#lub) {
      this.#lub = findLub(
        this.P, this.x, {...dOpt, less: this.less}
      )
    }

    return this.#lub
  }

  get exists() {
    const [l, u] = this.equalRange

    return (0 <= l) && (u < 0 || l < u)
  }

  get equalRange() {
    return [
      this.lowerBoundIndex,
      this.upperBoundIndex
    ]
  }
}

export default {
  findGlb,
  findLub,
  BinarySearch,
}

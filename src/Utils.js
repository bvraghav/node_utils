export function range(start, end=null, step=1) {
  if (end === null) {
    end = start
    start = 0
  }

  if (0 === step)
    throw Error(`Invalid step:${step}`)

  const n = Math.ceil((end - start) / step)

  return [...Array(n).keys()].map(
    (i) => (start + step * i)
  )
}

export function zip(...arrays) {
  const n = Math.min(...arrays.map(arr => arr.length))
  return range(n).map(
    (i) => arrays.map(arr => arr[i])
  )
}

export function unzip(arr) {
  return zip(...arr)
}

export function isType(obj, Type) {
  return (
    Type
      ? (obj?.constructor === Type)
      : obj === Type
  )
}

export function isArray(obj) {
  return isType(obj, Array)
}

export function isBoolean(obj) {
  return isType(obj, Boolean)
}

export function isString(obj) {
  return isType(obj, String)
}

export function isNumber(obj) {
  return isType(obj, Number)
}

export function isNull(obj) {
  return isType(obj, null)
}

export function isUndefined(obj) {
  return isType(obj, undefined)
}

export function isObject(obj) {
  return isType(obj, Object)
}


export default {
  range, unzip, zip,
  isType,
  isObject,
  isArray,
  isBoolean,
  isNumber,
  isString,
  isNull,
  isUndefined,
}

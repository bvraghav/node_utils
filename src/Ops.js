// BEWARE
// ----------------------------------------------------
// All ops are left associative
// ----------------------------------------------------

export const defineCompareOp = (
  binOp,
  nullaryDefault=true,
  unaryDefault=true
) => (...args) => {
  if (args.length < 1)
    return nullaryDefault
  if (args.length < 2)
    return unaryDefault

  let prev = null
  for (const arg of args) {
    if (prev != null && ! (prev < arg))
      return false
    prev = arg
  }
  return true
}

// ----------------------------------------------------
// Comparison
// ----------------------------------------------------
export const binLt = (a, b) => (a < b)
export const lt = defineCompareOp(binLt)

export const binLe = (a, b) => (a <= b)
export const le = defineCompareOp(binLt)

export const binEq = (a, b) => (a == b)
export const eq = defineCompareOp(binEq)

export const binEqq = (a, b) => (a === b)
export const eqq = defineCompareOp(binEqq)

export const binNe = (a, b) => (a != b)
export const ne = defineCompareOp(binNe)

export const binNne = (a, b) => (a !== b)
export const nne = defineCompareOp(binNne)

export const binGt = (a, b) => (a > b)
export const gt = defineCompareOp(binGt)

export const binGe = (a, b) => (a >= b)
export const ge = defineCompareOp(binGe)
// ----------------------------------------------------


// ----------------------------------------------------
// Bitwise
// ----------------------------------------------------
export const binBitOr = (a, b) => (a | b)
// export const bitOr = defineCompareOp(binBitOr)

export const binBitAnd = (a, b) => (a & b)
// export const bitAnd = defineCompareOp(binBitAnd)

export const binBitXor = (a, b) => (a ^ b)
// export const bitXor = defineCompareOp(binBitXor)
// ----------------------------------------------------

// ----------------------------------------------------
// Logical
// ----------------------------------------------------
export const binOr = (a, b) => (a || b)
export const or = defineCompareOp(binOr)

export const binAnd = (a, b) => (a && b)
export const and = defineCompareOp(binAnd)
// ----------------------------------------------------


// ----------------------------------------------------
// Arithmetic
// ----------------------------------------------------
export const binAdd = (a, b) => (a + b)
export const add = (...args) => args.reduce(binAdd)

export const binMul = (a, b) => (a * b)
export const mul = (...args) => args.reduce(binMul)

export const binPow = (a, b) => (a ** b)
export const pow = (...args) => args.reduce(binPow)

export const binMinus = (a, b) => (a - b)
export const minus = (...args) => args.reduce(binMinus)

export const binDivInt = (a, b) => (Math.floor(a / b))
export const divInt = (...args) => args.reduce(binDivInt)

export const binDivFloat = (a, b) => (a / b)
export const divFloat = (...args) => args.reduce(binDivFloat)
export const binDiv = binDivFloat
export const div = divFloat

export const binLog = (a, b) => div(Math.log(a), Math.log(b))
export const log = (...args) => args.reduce(binLog)
export const binRoot = (a, b) => (a ** div(1,b))
export const root = (...args) => args.reduce(binRoot)
// ----------------------------------------------------

export default {
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr, // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
  binAdd, add,
  binMul, mul,
  binPow, pow,
  binMinus, minus,
  binDivInt, divInt,
  binDivFloat, divFloat,
  binDiv, div,
  binLog, log,
  binRoot, root,
}

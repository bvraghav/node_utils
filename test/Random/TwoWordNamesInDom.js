/**
 * @vitest-environment jsdom
 */

import {describe, test, it, expect} from 'vitest'
import {camelCase} from 'change-case'
import {twoWordNames, twoWordName} from '#src/Random/TwoWordNames'

async function testName(w) {
  const n = w.split('_').length
  expect(n).toBe(2)

  const [a, b] = w.split('_')
  expect(a).toBe(camelCase(a))
  expect(b).toBe(camelCase(b))
}

// const { Symbol } = await import ('#/Symbol.js')

describe('Randomised two word names',  () => {
  it(
    'should have exactly two camelCased words separated by underscore',
    async () => {
      (await twoWordNames(10000))
        .forEach(testName)
    },
    10000,
  )
})

describe('Randomised two word name',  () => {
  it(
    'should be exactly One String, that satisfies testName',
    async () => {
      (await Promise.all(
        [...Array(10000)]
          .map(async () => (await twoWordName()))
      ))
        .forEach(testName)
    },
    10000,
  )
})

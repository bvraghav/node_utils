import {describe, test, it, expect} from 'vitest'

import {MersenneTwister} from '#src/Random/MersenneTwister.js'

import {BoxMuller} from '#src/Random/BoxMuller.js'

const stream = (sampler) => (
  [...Array(100000)].map(() => sampler.normal())
)

const iter = (seed) => {
  const X = stream(new BoxMuller(seed))
  const _sq = (x) => (x*x)
  const sq = (S) => S.map(_sq)
  const _sum = (a, b) => (a+b)
  const E = (S) => (S.reduce(_sum) / S.length)
  const meanX = E(X)
  const varX = E(sq(X)) - _sq(meanX)
  return {meanX, varX}
}

describe('BoxMuller', () => {
  it('yields a number', () => {
    expect((new BoxMuller()).normal()).toBeTypeOf('number')
  })


  it('normal: Zero mean and var one', () => {
    const _sum = (a, b) => (a+b)
    const E = (S) => (S.reduce(_sum) / S.length)

    const prng = new MersenneTwister()
    const seeds = [...Array(500)].map(() =>
      prng.genrand_int32())

    const iRes = seeds.map((N) => iter(N))
    const meanX = E(iRes.map(({meanX}) => meanX))
    const varX = E(iRes.map(({varX}) => varX))

    expect(meanX).toBeCloseTo(0)
    expect(varX).toBeCloseTo(1)
  })
})

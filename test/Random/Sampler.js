// class Sampler {
//   random()
//   normal()  
//   randInt()
//   randRange(a, bExclusive) 
//   randRangeIn(a, b)
//   sample(seq, {W, k, rep}={k:1, rep:false})
//   sampleOne(seq, {W})
//   sampleIid(seq, {W, k})
//   choice(seq)
//   shuffle(seq)
//   sampleUniformIid(seq, k)
//   sampleUniform(seq, k)
// }

import {describe, test, it, expect} from 'vitest'

import {MersenneTwister} from '#src/Random/MersenneTwister'
import {Sampler} from '#src/Random/Sampler'

const seed = (new MersenneTwister()).genrand_int32()
const sampler = new Sampler(seed)
const stream = (sampler, fname, ...fargs) => (
  [...Array(100000)].map(() => sampler[fname](...fargs))
)

describe('Sampler Sugar', () => {

  it('random() yields a number in [0,1)', () => {
    stream(sampler, 'random').forEach(
      (x) => {
        expect(x).toBeTypeOf('number')
        expect(0).toBeLessThanOrEqual(x)
        expect(x).toBeLessThan(1)
      }
    )
  })

  it('normal() yields a number', () => {
    stream(sampler, 'normal').forEach(
      (x) => {
        expect(x).toBeTypeOf('number')
      }
    )
  })

  it('shuffle() does shuffle', () => {
    const seq = [...Array(100000).keys()]
    const shuffled = sampler.shuffle(seq)

    expect(shuffled.length).toBe(seq.length)

    expect(seq).toMatchObject(
      shuffled.sort((a,b)=>(a-b))
    )
  })

  it('sampleUniform() gets k random samples', () => {
    const seq = [...Array(100000).keys()]
    const k = 10

    const [a, b, c] = [...Array(3)].map(
      () => {
        const s = sampler.sampleUniform(seq, k)
        expect(s).toHaveLength(k)
        expect(s).not.toMatchObject(seq.slice(0,k))
        expect(s).not.toMatchObject(seq.slice(-k))
        return s
      }
    )

    expect(a).not.toMatchObject(b)
    expect(b).not.toMatchObject(c)
    expect(a).not.toMatchObject(c)
  })

})

// ----------------------------------------------------
// In-source ViTest
// ----------------------------------------------------
import {describe, it, expect} from 'vitest'

import {
  findGlb, findLub, BinarySearch,
} from '#src/Algo/BinarySearch'

// const { Symbol } = await import ('#src/Symbol.js')

describe('Find GLB', () => {
  it.each([
    [[0,1,2],  1, 1],
    [[1,2,3],  1, 0],
    [[-1,0,1], 1, 2],
    [[-1,0,1], -1, 0],
    [[-1,0,1], 2, -1],
    [[1,2,2,3,4], 0, 0],
    [[1,2,2,3,4], 1, 0],
    [[1,2,2,3,4], 1.5, 1],
    [[1,2,2,3,4], 2, 1],
    [[1,2,2,3,4], 2.5, 3],
    [[1,2,2,3,4], 3.5, 4],
    [[1,2,2,3,4], 4, 4],
    [[1,2,2,3,4], 4.5, -1],
  ])('Expect (%j, %d) => %d', (A, x, p) => {
    expect(findGlb(A,x)).toBe(p)
  })
})

describe('Find LUB', () => {
  it.each([
    [[0,1,2],  1, 2],
    [[1,2,3],  1, 1],
    [[-1,0,1], 1, -1],
    [[-1,0,1], -1, 1],
    [[-1,0,1], -2, 0],
    [[1,2,2,3,4], 0, 0],
    [[1,2,2,3,4], 1, 1],
    [[1,2,2,3,4], 1.5, 1],
    [[1,2,2,3,4], 2, 3],
    [[1,2,2,3,4], 2.5, 3],
    [[1,2,2,3,4], 3.5, 4],
    [[1,2,2,3,4], 4, -1],
    [[1,2,2,3,4], 4.5, -1],
  ])('Expect (%j, %d) => %d', (A, x, p) => {
    expect(findLub(A,x)).toBe(p)
  })
})

describe('Initialise BinarySearch trivial', () => {
  it('init', () => {
    const B = new BinarySearch([0,1,2], 1)
    expect(B.P).toStrictEqual([0,1,2])
    expect(B.x).toBe(1)
    
    expect(B.lowerBoundIndex).toBe(1)
    expect(B.upperBoundIndex).toBe(2)
    expect(B.exists).toBe(true)
    expect(B.equalRange).toStrictEqual([1,2])
  })
})

describe('BinarySearch', () => {
  it.each([
    [[0,1,2],  1, 1,2, true],
    [[1,2,3],  1, 0,1, true],
    [[-1,0,1], 1, 2,-1, true],
    [[-1,0,1], -1, 0,1, true],
    [[-1,0,1], 2, -1,-1, false],
    [[1,2,2,3,4], 0, 0,0, false],
    [[1,2,2,3,4], 1, 0,1, true],
    [[1,2,2,3,4], 1.5, 1,1, false],
    [[1,2,2,3,4], 2, 1,3, true],
    [[1,2,2,3,4], 2.5, 3,3, false],
    [[1,2,2,3,4], 3.5, 4,4, false],
    [[1,2,2,3,4], 4, 4, -1, true],
    [[1,2,2,3,4], 4.5, -1,-1, false],
  ])('Expect: (%j,%d) => l:%i,u:%i, e:%s', (A, x, l,u,e) => {
    const B = new BinarySearch(A, x)
    expect(B.lowerBoundIndex).toBe(l)
    expect(B.upperBoundIndex).toBe(u)
    expect(B.exists).toBe(e)
    expect(B.equalRange).toStrictEqual([l, u])
  })
})

// ----------------------------------------------------


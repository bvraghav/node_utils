import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    include: ['test/**/*.{js,ts}'],
  },
  resolve: {
    alias: {
      '#src': './src',
    }
  },
  testTimeout: 2000,
})
